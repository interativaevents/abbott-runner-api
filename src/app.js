import http from 'http'
import { env, mongo, port, ip } from './config'
import mongoose from './services/mongoose'
import express from './services/express'
import api from './api'

const app = express(api)
const server = http.createServer(app)

//Implementation of socket.io with redis
var io = require('socket.io')(server);
var redis = require('socket.io-redis');

io.adapter(redis({ host: '127.0.0.1', port: '6379' }));

mongoose.connect(mongo.uri)

setImmediate(() => {
  server.listen(port, ip, () => {
    console.log('Express server listening on http://%s:%d, in %s mode', ip, port, env)
  })
})

export default app
