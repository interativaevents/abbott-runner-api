import _ from 'lodash'
import { success, notFound } from '../../services/response/'
import { Question } from '.'
import { Team } from '../team'

//remove as alternativas vazias e adiciona um numero para cada
var formatAlternatives = (alternativesDirty) => {
  return alternativesDirty.map((alternative, index) => {
    if (!!alternative.title) {
      alternative.number = index;
      return alternative;
    }
  })
}

export const create = ({ body }, res, next) => {

  body.alternatives = formatAlternatives(body.alternatives)
  let query = { city: body.city }

  Question.create(body)
    .then((question) => {
      return new Promise((resolse, reject) => {
        //Adiciona o id da questão no campo questios dos  Times da cidade desta questão.
        Team.find(query)
          .then(teams => {
            teams.forEach(team => {
              team.questions.push(question._id)
              team.save()
            })
            resolse(question)
          })
          .catch(err => {
            question.remove()
            reject(err)
          })

      })
    })
    .then(success(res, 201))
    .catch(next)
}


export const createMany = ({ body }, res, next) => {
  let questions = [];
  body.forEach(question => {
    question.alternatives = formatAlternatives(question.alternatives)
    let query = { city: question.city }

    Question.create(question)
      .then((question) => {
        return new Promise((resolse, reject) => {
          //Adiciona o id da questão no campo questios dos  Times da cidade desta questão.
          Team.find(query)
            .then(teams => {
              teams.forEach(team => {
                team.questions.push(question._id)
                team.save()
              })
              resolse(question)
            })
            .catch(err => {
              question.remove()
              reject(err)
            })

        })
      })
      .then(questionInserted => questions.push(questionInserted))
      .catch(err => console.log(err))

  })
  res.status(200).json(questions);

}




export const getQuestionsByCityId = ({ querymen: { query, select, cursor }, params }, res, next) => {
  console.log(select)
  Question.find(query, select, cursor)
    .then((questions) => questions.map((question) => question.view()))
    .then(success(res))
    .catch(next)

}





export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Question.find(query, select, cursor)
    .then((questions) => questions.map((question) => question.view()))
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  Question.findById(params.id)
    .then(notFound(res))
    .then((question) => question ? question.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ body, params }, res, next) =>
  Question.findById(params.id)
    .then(notFound(res))
    .then((question) => {
      if (!question.answered) {
        question.alternatives = formatAlternatives(body.alternatives)
        return question ? _.merge(question, body).save() : null
      } else {
        console.log("QUESTION can't be updated because already answered", question._id)
        return question
      }
    })
    .then((question) => question ? question.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) => {

  Question.findById(params.id)
    .then(notFound(res))
    .then((question) => {
      if (question.answered) {
        console.log("QUESTION can't be removed because already answered", question._id)
      } else {
        question ? question.remove() : null
        //removedo a questão deletada nas questios dos temes que a contem
        Team.find({ city: question.city })
          .then(teams => {
            teams.forEach(team => {
              let index = team.questions.indexOf(params.id)
              if (index > -1) {
                team.questions.splice(index, 1);
              }
              team.save()
            })
          })
        console.log("QUESTION REMOVED", params.id)
      }
      return question;

    })
    .then(success(res, 204))
    .catch(next)
}

export const findByCityId = ({ params }, res, next) => {
  let query = {
    city: params.cityId
  }

  Question.find(query)
    .populate('city')
    .sort("createdAt")
    .exec()
    .then((questions) => questions.map((question) => question.view()))
    .then(success(res, 202))
    .catch(next)
}

export const findByTeamId = ({ params }, res, next) => {

  Team.findById({ _id: params.teamId })
    .populate('questions')
    .exec()
    .then(notFound(res))
    .then(team => {
      if(!!team.questions.length){
        return team.questions.sort(orderByCreatedAt)[0].view()
      }else{
        return {};
      }
    })
    .then(success(res, 202))
    .catch(next)
}

function orderByCreatedAt(a, b) {
  if (a.createdAt < b.createdAt)
    return -1;
  if (a.createdAt > b.createdAt)
    return 1;
  return 0;
}