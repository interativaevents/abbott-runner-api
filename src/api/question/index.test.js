import request from 'supertest-as-promised'
import { signSync } from '../../services/jwt'
import express from '../../services/express'
import { User } from '../user'
import routes, { Question } from '.'

const app = () => express(routes)

let userSession, adminSession, question

beforeEach(async () => {
  const user = await User.create({ email: 'a@a.com', password: '123456' })
  const admin = await User.create({ email: 'c@c.com', password: '123456', role: 'admin' })
  userSession = signSync(user.id)
  adminSession = signSync(admin.id)
  question = await Question.create({})
})

test('POST /questions 201 (admin)', async () => {
  const { status, body } = await request(app())
    .post('/')
    .send({ access_token: adminSession, city: 'test', title: 'test', value: 'test', alternatives: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.city).toEqual('test')
  expect(body.title).toEqual('test')
  expect(body.value).toEqual('test')
  expect(body.alternatives).toEqual('test')
})

test('POST /questions 401 (user)', async () => {
  const { status } = await request(app())
    .post('/')
    .send({ access_token: userSession })
  expect(status).toBe(401)
})

test('POST /questions 401', async () => {
  const { status } = await request(app())
    .post('/')
  expect(status).toBe(401)
})

test('GET /questions 200', async () => {
  const { status, body } = await request(app())
    .get('/')
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /questions/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`/${question.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(question.id)
})

test('GET /questions/:id 404', async () => {
  const { status } = await request(app())
    .get('/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /questions/:id 200 (admin)', async () => {
  const { status, body } = await request(app())
    .put(`/${question.id}`)
    .send({ access_token: adminSession, city: 'test', title: 'test', value: 'test', alternatives: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(question.id)
  expect(body.city).toEqual('test')
  expect(body.title).toEqual('test')
  expect(body.value).toEqual('test')
  expect(body.alternatives).toEqual('test')
})

test('PUT /questions/:id 401 (user)', async () => {
  const { status } = await request(app())
    .put(`/${question.id}`)
    .send({ access_token: userSession })
  expect(status).toBe(401)
})

test('PUT /questions/:id 401', async () => {
  const { status } = await request(app())
    .put(`/${question.id}`)
  expect(status).toBe(401)
})

test('PUT /questions/:id 404 (admin)', async () => {
  const { status } = await request(app())
    .put('/123456789098765432123456')
    .send({ access_token: adminSession, city: 'test', title: 'test', value: 'test', alternatives: 'test' })
  expect(status).toBe(404)
})

test('DELETE /questions/:id 204 (admin)', async () => {
  const { status } = await request(app())
    .delete(`/${question.id}`)
    .query({ access_token: adminSession })
  expect(status).toBe(204)
})

test('DELETE /questions/:id 401 (user)', async () => {
  const { status } = await request(app())
    .delete(`/${question.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(401)
})

test('DELETE /questions/:id 401', async () => {
  const { status } = await request(app())
    .delete(`/${question.id}`)
  expect(status).toBe(401)
})

test('DELETE /questions/:id 404 (admin)', async () => {
  const { status } = await request(app())
    .delete('/123456789098765432123456')
    .query({ access_token: adminSession })
  expect(status).toBe(404)
})
