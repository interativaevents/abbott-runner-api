import { Question } from '.'

let question

beforeEach(async () => {
  question = await Question.create({ city: 'test', title: 'test', value: 'test', alternatives: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = question.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(question.id)
    expect(view.city).toBe(question.city)
    expect(view.title).toBe(question.title)
    expect(view.value).toBe(question.value)
    expect(view.alternatives).toBe(question.alternatives)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = question.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(question.id)
    expect(view.city).toBe(question.city)
    expect(view.title).toBe(question.title)
    expect(view.value).toBe(question.value)
    expect(view.alternatives).toBe(question.alternatives)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
