import mongoose, { Schema } from 'mongoose'

const alternativeSchema = new Schema({
  title: String,
  number:String,
  isRight: Boolean
});

const questionSchema = new Schema({
  city: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'City'
  },
  title: String,
  weight: {
    type: Number,
    default: 0
  },
  answered: {
    type: Boolean,
    default: false
  },
  alternatives: [alternativeSchema]
}, {
    timestamps: true
  })


questionSchema.methods = {
  view(full) {
    const view = {
      // simple view
      id: this.id,
      city: this.city,
      title: this.title,
      weight: this.weight,
      alternatives: this.alternatives,
      answered: this.answered,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}



const model = mongoose.model('Question', questionSchema)

export const schema = model.schema
export default model
