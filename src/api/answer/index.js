import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { create, index, show, update, destroy, getScoresByCity } from './controller'
import { schema } from './model'
export Answer, { schema } from './model'

const router = new Router()
const { userId, questionId, alternativeId,alternativeNumber, teamId, cityId, time, itens, score, isRight } = schema.tree

/**
 * @api {post} /answers Create answer
 * @apiName CreateAnswer
 * @apiGroup Answer
 * @apiParam userId Answer's userId.
 * @apiParam questionId Answer's questionId.
 * @apiParam alternativeId Answer's alternativeId.
 * @apiParam teamId Answer's teamId
 * @apiParam cityId Answer's cityId 
 * @apiParam time Answer's time 
 * @apiParam itens Answer's itens 
 * @apiSuccess {Object} answer Answer's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Answer not found.
 */
router.post('/',
  body({ userId, questionId, alternativeId, teamId, cityId, time, itens }),
  create)

/**
 * @api {get} /answers/score/:cityId Retrieve scores by city
 * @apiName RetriveScoresByCity
 * @apiGroup Scores
 * @apiUse listParams
 * @apiSuccess {Object[]} scores od two teams.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/score/:cityId',
  getScoresByCity)

/**
 * @api {get} /answers Retrieve answers
 * @apiName RetrieveAnswers
 * @apiGroup Answer
 * @apiUse listParams
 * @apiSuccess {Object[]} answers List of answers.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /answers/:id Retrieve answer
 * @apiName RetrieveAnswer
 * @apiGroup Answer
 * @apiSuccess {Object} answer Answer's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Answer not found.
 */
router.get('/:id',
  show)

/**
 * @api {put} /answers/:id Update answer
 * @apiName UpdateAnswer
 * @apiGroup Answer
 * @apiParam userId Answer's userId.
 * @apiParam questionId Answer's questionId.
 * @apiParam alternativeId Answer's alternativeId.
 * @apiParam teamId Answer's teamId
 * @apiParam cityId Answer's cityId 
 * @apiParam time Answer's time 
 * @apiParam itens Answer's itens 
 * @apiSuccess {Object} answer Answer's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Answer not found.
 */
router.put('/:id',
  body({ userId, questionId, alternativeId, time, teamId, cityId, itens }),
  update)

/**
 * @api {delete} /answers/:id Delete answer
 * @apiName DeleteAnswer
 * @apiGroup Answer
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Answer not found.
 */
router.delete('/:id',
  destroy)

export default router
