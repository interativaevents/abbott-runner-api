import { Answer } from '.'

let answer

beforeEach(async () => {
  answer = await Answer.create({ userId: 'test', questionId: 'test', alternativeId: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = answer.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(answer.id)
    expect(view.userId).toBe(answer.userId)
    expect(view.questionId).toBe(answer.questionId)
    expect(view.alternativeId).toBe(answer.alternativeId)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = answer.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(answer.id)
    expect(view.userId).toBe(answer.userId)
    expect(view.questionId).toBe(answer.questionId)
    expect(view.alternativeId).toBe(answer.alternativeId)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
