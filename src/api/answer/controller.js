import _ from 'lodash'
import { success, notFound } from '../../services/response/'
import { Question } from '../question'
import { User } from '../user'
import { Team } from '../team'
import { Answer } from '.'
import mongoose from 'mongoose'


var io;
if (!!process.env.REDISCLOUD_URL) {
  io = require('socket.io-emitter')(process.env.REDISCLOUD_URL);
} else {
  io = require('socket.io-emitter')({ host: '127.0.0.1', port: 6379 });
}

export const create = ({ body }, res, next) => {
  console.log(body)
  let userId = body.userId
  let alternativeId = body.alternativeId
  let questionId = body.questionId
  let teamId = body.teamId
  let cityId = body.cityId
  let time = body.time
  let curretScore = body.score
  let itens = body.itens
  let answerUser;

  let user = {}

  Answer.create(body)
    .then(answerInserted => {
      answerUser = answerInserted;
      return Question.findById(questionId)
    })
    .then(notFound(res))
    .then(question => verifyAnswerIsRight(time, itens, curretScore, alternativeId, userId, question, answerUser))
    .then(() => setQuestionAsAnswered(questionId))//seta a questão como respondida
    .then(() => removeQuestionOfTeam(teamId, questionId))//remove questão do time
    .then(() => findUserUpdated(userId))//busca o usuario atualizado para pegar seu score atual
    .then(user => {
      returnScoreOfUser(user, res)
    })//envia o score do usurio que respondeu a pergunta e os scores dos times desse usuario
    .catch(next)
}


var verifyAnswerIsRight = (time, itens, curretScore, alternativeId, userId, question, answerUser) => {
  let update = {
    time: time,
    itens: itens,
    answer: answerUser._id.toString(),
    $inc: {
      score: curretScore
    }
  }
  //Verifica se a alternativa respondidad esta correta e 
  //se SIM incrementa o valor da questão no score do usuario
  answerUser.score = curretScore;
  for (let i = 0; i < question.alternatives.length; i++) {
    let alternative = question.alternatives[i]
    if (alternative._id == alternativeId) {

      answerUser.alternativeNumber = alternative.number;

      if (alternative.isRight) {
        let fullScore = question.weight + curretScore;
        answerUser.isRight = true;
        answerUser.score = fullScore;
        update.$inc = { score: fullScore }
      }

      answerUser.save();
    }
  }

  return User.findByIdAndUpdate(userId, update).exec()

}

//remove o id da questão respondida das questoes do time
var removeQuestionOfTeam = (teamId, questionId) => Team.findByIdAndUpdate(teamId, { $pull: { questions: questionId } }).exec()

//seta a questão como respondida
var setQuestionAsAnswered = (questionId) => Question.findByIdAndUpdate(questionId, { answered: true }).exec();

//busca o usuario atualizado para pegar seu score atual
var findUserUpdated = (userId) => User.findById(userId).populate('city team').exec();

//envia o score do usurio que respondeu a pergunta e os scores dos times desse usuario
var returnScoreOfUser = ({ team, city, score}, res) => {
  sendScore(team._id, team.name, city.name)//gera e emite score do time do  usurio que respondeu a pergunta
  res.status(202).json({ score: score })//envia o score do usurio que respondeu a pergunta
}

//soma os pontos de todos os usuarios de um determinado time
var sendScore = (teamId, teamName, cityName) => {

  User.aggregate([
    {
      $match: {
        team: new mongoose.mongo.ObjectId(teamId.toString())
      }
    },
    {
      $group: {
        _id: '$team', score: { $sum: '$score' }
      }
    }
  ])
    .then(result => {
      let data = {
        city: cityName,
        team: teamName,
        score: 0
      }
      if (!!result.length) {
        data.score = result[0].score
      }
      console.log("UpdateScore", data)
      //dispara o evento para o cliente socket.io com o score do time    
      io.emit('UpdateScore', data)
    })
    .catch(err => console.log("Error on Aggregate: ", err))//end Aggregate

}

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Answer.find(query, select, cursor)
    .then((answers) => answers.map((answer) => answer.view()))
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  Answer.findById(params.id)
    .then(notFound(res))
    .then((answer) => answer ? answer.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  Answer.findById(params.id)
    .then(notFound(res))
    .then((answer) => answer ? _.merge(answer, body).save() : null)
    .then((answer) => answer ? answer.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  Answer.findById(params.id)
    .then(notFound(res))
    .then((answer) => answer ? answer.remove() : null)
    .then(success(res, 204))
    .catch(next)

export const getScoresByCity = ({ params }, res, next) => {
  Team.find({ city: params.cityId })
    .populate('city')
    .exec()
    .then(teams => {
      teams.forEach(team => sendScore(team._id, team.name, team.city.name))
      res.status(200).json(true)
    })
    .catch(next)

}
