import mongoose, { Schema } from 'mongoose'

const answerSchema = new Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  questionId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Question'
  },
  alternativeId: {
    type: mongoose.Schema.Types.ObjectId,
  },
  alternativeNumber: String,
  isRight: {
    type: Boolean,
    default: false
  },
  teamId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Team'
  },
  cityId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'City'
  },
  time: {
    type: Number
  },
  itens: [Number],
  score: Number
}
  , {
    timestamps: true
  })

answerSchema.methods = {
  view(full) {
    const view = {
      // simple view
      id: this.id,
      userId: this.userId,
      questionId: this.questionId,
      alternativeId: this.alternativeId,
      alternativeNumber: this.alternativeNumber,
      isRight: this.isRight,
      cityId: this.cityId,
      teamId: this.teamId,
      time: this.time,
      itens: this.itens,
      score: this.score,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Answer', answerSchema)

export const schema = model.schema
export default model

