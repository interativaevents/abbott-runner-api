import mongoose, { Schema } from 'mongoose'

const teamSchema = new Schema({
  name: {
    type: String
  },
  number: Number,
  city: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'City'
  },
  questions: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Question'
  }]
}, {
    timestamps: true
  })

teamSchema.methods = {
  view(full) {
    const view = {
      // simple view
      id: this.id,
      name: this.name,
      number: this.number,
      city: this.city,
      questions: this.questions,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

teamSchema.statics.findAndModify = function (query, sort, doc, options, callback) {
  return this.collection.findAndModify(query, sort, doc, options, callback);
};

const model = mongoose.model('Team', teamSchema)

export const schema = model.schema
export default model
