import { Team } from '.'

let team

beforeEach(async () => {
  team = await Team.create({ name: 'test', city: 'test', questions: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = team.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(team.id)
    expect(view.name).toBe(team.name)
    expect(view.city).toBe(team.city)
    expect(view.questions).toBe(team.questions)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = team.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(team.id)
    expect(view.name).toBe(team.name)
    expect(view.city).toBe(team.city)
    expect(view.questions).toBe(team.questions)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
