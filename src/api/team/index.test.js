import request from 'supertest-as-promised'
import express from '../../services/express'
import routes, { Team } from '.'

const app = () => express(routes)

let team

beforeEach(async () => {
  team = await Team.create({})
})

test('POST /teams 201', async () => {
  const { status, body } = await request(app())
    .post('/')
    .send({ name: 'test', city: 'test', questions: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.name).toEqual('test')
  expect(body.city).toEqual('test')
  expect(body.questions).toEqual('test')
})

test('GET /teams 200', async () => {
  const { status, body } = await request(app())
    .get('/')
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /teams/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`/${team.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(team.id)
})

test('GET /teams/:id 404', async () => {
  const { status } = await request(app())
    .get('/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /teams/:id 200', async () => {
  const { status, body } = await request(app())
    .put(`/${team.id}`)
    .send({ name: 'test', city: 'test', questions: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(team.id)
  expect(body.name).toEqual('test')
  expect(body.city).toEqual('test')
  expect(body.questions).toEqual('test')
})

test('PUT /teams/:id 404', async () => {
  const { status } = await request(app())
    .put('/123456789098765432123456')
    .send({ name: 'test', city: 'test', questions: 'test' })
  expect(status).toBe(404)
})

test('DELETE /teams/:id 204', async () => {
  const { status } = await request(app())
    .delete(`/${team.id}`)
  expect(status).toBe(204)
})

test('DELETE /teams/:id 404', async () => {
  const { status } = await request(app())
    .delete('/123456789098765432123456')
  expect(status).toBe(404)
})
