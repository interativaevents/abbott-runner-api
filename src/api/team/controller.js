import _ from 'lodash'
import { success, notFound } from '../../services/response/'
import { Team } from '.'

export const create = ({ bodymen: { body } }, res, next) =>
  Team.create(body)
    .then((team) => team.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Team.find(query, select, cursor)
    .then((teams) => teams.map((team) => team.view()))
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  Team.findById(params.id)
    .then(notFound(res))
    .then((team) => team ? team.view() : null)
    .then(success(res))
    .catch(next)

export const getTeamByCity = ({ params }, res, next) =>
  Team.find({ city: params.cityId })
    .then(notFound(res))
    .then((teams) => teams.map((team) => team.view()))
    .then(success(res))
    .catch(next)

export const hasQuestionsInTeam = ({ params }, res, next) =>
  Team.findById(params.teamId)
    .then(notFound(res))
    .then((team) => res.status(202).json(!team.questions.length))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  Team.findById(params.id)
    .then(notFound(res))
    .then((team) => team ? _.merge(team, body).save() : null)
    .then((team) => team ? team.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  Team.findById(params.id)
    .then(notFound(res))
    .then((team) => team ? team.remove() : null)
    .then(success(res, 204))
    .catch(next)

export const hasQuestionsInCity = ({ params }, res, next) => {
  Team.find({ city: params.cityId })
    .then(notFound(res))
    .then((teams) => {
      console.log("A - "+!teams[0].questions.length+" B - "+!teams[1].questions.length);
      if (!teams[0].questions.length && !teams[1].questions.length) {
        res.status(200).json(true)
      } else {
        res.status(200).json(false)
      }
    }
    )
    .catch(next)
}    
