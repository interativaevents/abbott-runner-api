import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { create, index, show, update, destroy, getTeamByCity, hasQuestionsInCity, hasQuestionsInTeam } from './controller'
import { schema } from './model'
export Team, { schema } from './model'

const router = new Router()
const { name, number, city, questions } = schema.tree

/**
 * @api {post} /teams Create team
 * @apiName CreateTeam
 * @apiGroup Team
 * @apiParam name Team's name.
 * @apiParam city Team's city.
 * @apiParam questions Team's questions.
 * @apiSuccess {Object} team Team's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Team not found.
 */
router.post('/',
  body({ name, city, questions, number }),
  create)

/**
 * @api {get} /teams Retrieve teams
 * @apiName RetrieveTeams
 * @apiGroup Team
 * @apiUse listParams
 * @apiSuccess {Object[]} teams List of teams.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /teams/:id Retrieve team
 * @apiName RetrieveTeam
 * @apiGroup Team
 * @apiSuccess {Object} team Team's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Team not found.
 */
router.get('/:id',
  show)

/**
 * @api {get} /teams/isdone/:teamId verify if team have questions by Team id
 * @apiName HasQuestionsInTeam
 * @apiGroup Team
 * @apiSuccess {Object} true if has questions in team or false if not.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Team not found.
 */
router.get('/isdone/:teamId',
  hasQuestionsInTeam)

/**
 * @api {get} /teams/iscomplete/:cityId verify if team have questions by City id
 * @apiName HasQuestionsInTeam
 * @apiGroup Team
 * @apiSuccess {Object} true if has questions im team or false if not.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Team not found.
 */
router.get('/iscomplete/:cityId',
  hasQuestionsInCity)


/**
 * @api {get} /teams/city/:cityId Retrieve team give a City ID
 * @apiName RetrieveTeam
 * @apiGroup Team
 * @apiSuccess {Object} team Team's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Team not found.
 */
router.get('/city/:cityId',
  getTeamByCity)

/**
 * @api {put} /teams/:id Update team
 * @apiName UpdateTeam
 * @apiGroup Team
 * @apiParam name Team's name.
 * @apiParam city Team's city.
 * @apiParam questions Team's questions.
 * @apiSuccess {Object} team Team's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Team not found.
 */
router.put('/:id',
  body({ name, number, city, questions }),
  update)

/**
 * @api {delete} /teams/:id Delete team
 * @apiName DeleteTeam
 * @apiGroup Team
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Team not found.
 */
router.delete('/:id',
  destroy)

export default router
