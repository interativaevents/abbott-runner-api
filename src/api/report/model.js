import mongoose, { Schema } from 'mongoose'

const answerSchema = new Schema({
    user: String,
    role:String,
    team_name: String,
    city_name: String,
    score: Number,    
    question_title: Object,
    alternativeId: {
        type: mongoose.Schema.Types.ObjectId,
    },
    question_alternatives:[Object],
    isRight: {
        type: Boolean,
        default: false
    },
    itens: [Number]
})


const model = mongoose.model('Reports', answerSchema)
export default model

