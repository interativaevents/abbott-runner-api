
import _ from 'lodash'
import { success, notFound } from '../../services/response/'
import { Question } from '../question'
import { User } from '../user'
import { Team } from '../team'
import { Answer } from '../answer'
import { City } from '../city'
import Report from './model'
import mongoose from 'mongoose'

export const index = ({ querymen: { query, select, cursor } }, res, next) => {
  getReport()
    .then(result => res
      .status(200)
      .json(mapReport(result)))

}

export const show = ({ params }, res, next) => {
  Report.find()
    .then(reports => res.status(200).json(mapReport(reports)))
}

var getReport = () => {
  return Answer.aggregate([
    //--Users
    {
      $lookup:
      {
        from: "users",
        localField: "userId",
        foreignField: "_id",
        as: "user"
      }

    }
    ,//--Questions
    {
      $lookup:
      {
        from: "questions",
        localField: "questionId",
        foreignField: "_id",
        as: "question"
      }
    }
    ,//--Cities
    {
      $lookup:
      {
        from: "cities",
        localField: "cityId",
        foreignField: "_id",
        as: "city_name"
      }
    },

    {
      $match: {
        "city_name.name": { $regex: '^((?!TEST).)*$' }
      }
    }
    ,//Teams
    {
      $lookup:
      {
        from: "teams",
        localField: "teamId",
        foreignField: "_id",
        as: "team_name"
      }
    }
    ,//----
    {
      $project:
      {
        user:
        {
          $arrayElemAt: ["$user.name", 0]
        },
        score: 1,
        isRight: 1,
        alternativeNumber: 1,
        alternativeId: 1,
        cityId: 1,
        city_name:
        {
          $arrayElemAt: ["$city_name.name", 0]
        }
        ,
        team_name:
        {
          $arrayElemAt: ["$team_name.number", 0]
        }
        ,
        question_title:
        {
          $arrayElemAt: ["$question.title", 0]
        },
        question_alternatives:
        {
          $arrayElemAt: ["$question.alternatives", 0]
        }

      }
    },
    {
      $sort: { city_name: 1, team_name: 1, user: 1 }
    }

  ])

}

var mapReport = (reports) => {

  return reports.map(report => {
    report.alternative = report.question_alternatives.filter(alternative => report.alternativeId.toString() == alternative._id.toString())[0]
    delete report.question_alternatives;
    delete report.alternative._id;
    delete report.alternativeId;
    delete report._id;
    return report;
  })

}

export const reportsByQuestion = ({ querymen: { query, select, cursor }, params }, res, next) => {
  let match = {}
  if (params.cityId.length > 5) {
    match = {
      city: new mongoose.mongo.ObjectId(params.cityId)
    }
  }

  Question.aggregate([
    {
      $match: match
    },
    {
      $sort: { createdAt: -1 }
    },
    {
      $lookup: {
        from: 'cities',
        localField: 'city',
        foreignField: '_id',
        as: 'cities'
      }
    },
    {
      $lookup: {
        from: 'answers',
        localField: '_id',
        foreignField: 'questionId',
        as: 'answers'
      }
    },
    {
      $project: {
        "_id": 0,
        "title": 1,
        "alternatives.title": 1,
        "alternatives.number": 1,
        "alternatives.isRight": 1,
        "answers.isRight": 1,
        "answers.alternativeNumber": 1,
        city:
        {
          $arrayElemAt: ["$cities.name", 0]
        }
      }
    }
  ]).then(reports => res.json(reports))

}