import { Router } from 'express'
import { middleware as query } from 'querymen'
import { token } from '../../services/passport'
import { index, show,reportsByQuestion } from './controller'

const router = new Router()

/**
 * @api {get} /reports Retrieve reports
 * @apiName RetrieveReports
 * @apiGroup Report
 * @apiPermission admin
 * @apiParam {String} access_token admin access token.
 * @apiUse listParams
 * @apiSuccess {Object[]} reports List of reports.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 admin access only.
 */
router.get('/',
  // token({ required: true, roles: ['admin'] }),
   query(),
  index)


router.get('/question/:cityId',
  query(),
  reportsByQuestion)


  
/**
 * @api {get} /reports/:id Retrieve report
 * @apiName RetrieveReport
 * @apiGroup Report
 * @apiPermission admin
 * @apiParam {String} access_token admin access token.
 * @apiSuccess {Object} report Report's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Report not found.
 * @apiError 401 admin access only.
 */
router.get('/all',
 // token({ required: true, roles: ['admin'] }),
  show)

export default router
