import mongoose, { Schema } from 'mongoose'

const citySchema = new Schema({
  name: {
    type: String,
    unique: true
  }
}, {
    timestamps: true
  })

citySchema.methods = {
  view(full) {
    const view = {
      // simple view
      id: this.id,
      name: this.name,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('City', citySchema)

export const schema = model.schema
export default model
