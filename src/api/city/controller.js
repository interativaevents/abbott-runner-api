import _ from 'lodash'
import { success, notFound } from '../../services/response/'
import { City } from '.'
import { schema } from './model'
import { Team } from '../team'

/* {
 	"cities":[
	  { "name": "CHICAGO" },
	  { "name": "TORONTO" },
	  { "name": "CIDADE DO MEXICO" },
	  { "name": "ARGENTINA" },
	  { "name": "COLOMBIA" },
	  { "name": "MIAMI" },
	  { "name": "FLORIANOPOLIS" }
	]*/
// }
export const createMany = ({body}, res, next) => {
  let cities = [];
  City.create(body.cities)
    .then(cities => cities.map((city) => city.view()))  
    .then(citiesSaved => {
      console.log("Cities saved: ", citiesSaved)
      let teams = [];
      citiesSaved.forEach(city => {
        teams.push({ name: 'Team 1',number:1, city: city.id })
        teams.push({ name: 'Team 2',number:2, city: city.id })
      })
      return Team.create(teams)
    })
    .then(teams => teams.map((team) => team.view()))
    .then(success(res))
    .catch(next)
}

export const create = ({ bodymen: { body } }, res, next) => {
  let city = {};
  city.teams = []

  City.create(body)
    .then((savedCity) => savedCity.view(true))
    .then(savedCity => {
      city = savedCity;
      let teams = [{ name: 'Team 1',number:1, city: savedCity.id }, { name: 'Team 2',number:2, city: savedCity.id }]
      return Team.create(teams)
    })
    .then(teams => {
      city.teams = teams;
      return city;
    })
    .then(success(res, 201))
    .catch(err => res.status(501).json({ code: err.code, msg: 'Já existe uma cidade cadastrada com esse nome.' }))
}

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  City.find(query, select, cursor)
    .then((cities) => cities.map((city) => city.view()))
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  City.findById(params.id)
    .then(notFound(res))
    .then((city) => city ? city.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  City.findById(params.id)
    .then(notFound(res))
    .then((city) => city ? _.merge(city, body).save() : null)
    .then((city) => city ? city.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  City.findById(params.id)
    .then(notFound(res))
    .then((city) => city ? city.remove() : null)
    .then(success(res, 204))
    .catch(next)
