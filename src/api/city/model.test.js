import { City } from '.'

let city

beforeEach(async () => {
  city = await City.create({ name: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = city.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(city.id)
    expect(view.name).toBe(city.name)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = city.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(city.id)
    expect(view.name).toBe(city.name)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
