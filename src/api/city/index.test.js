import request from 'supertest-as-promised'
import express from '../../services/express'
import routes, { City } from '.'

const app = () => express(routes)

let city

beforeEach(async () => {
  city = await City.create({})
})

test('POST /cities 201', async () => {
  const { status, body } = await request(app())
    .post('/')
    .send({ name: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.name).toEqual('test')
})

test('GET /cities 200', async () => {
  const { status, body } = await request(app())
    .get('/')
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /cities/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`/${city.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(city.id)
})

test('GET /cities/:id 404', async () => {
  const { status } = await request(app())
    .get('/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /cities/:id 200', async () => {
  const { status, body } = await request(app())
    .put(`/${city.id}`)
    .send({ name: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(city.id)
  expect(body.name).toEqual('test')
})

test('PUT /cities/:id 404', async () => {
  const { status } = await request(app())
    .put('/123456789098765432123456')
    .send({ name: 'test' })
  expect(status).toBe(404)
})

test('DELETE /cities/:id 204', async () => {
  const { status } = await request(app())
    .delete(`/${city.id}`)
  expect(status).toBe(204)
})

test('DELETE /cities/:id 404', async () => {
  const { status } = await request(app())
    .delete('/123456789098765432123456')
  expect(status).toBe(404)
})
