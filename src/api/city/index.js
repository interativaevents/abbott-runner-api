import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { create, index, show, update, destroy, createMany } from './controller'
import { schema } from './model'
export City, { schema } from './model'

const router = new Router()
const { name } = schema.tree

/**
 * @api {post} /cities Create city
 * @apiName CreateCity
 * @apiGroup City
 * @apiParam name City's name.
 * @apiSuccess {Object} city City's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 City not found.
 */
router.post('/',
  body({ name }),
  create)

/**
 * @api {post} /cities/all Create many cities
 * @apiName CreateCities and Teams
 * @apiGroup City
 * @apiParam Names City's names.
 * @apiSuccess {Object} cities City's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 City not found.
 */
router.post('/all',
  createMany)

/**
 * @api {get} /cities Retrieve cities
 * @apiName RetrieveCities
 * @apiGroup City
 * @apiUse listParams
 * @apiSuccess {Object[]} cities List of cities.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /cities/:id Retrieve city
 * @apiName RetrieveCity
 * @apiGroup City
 * @apiSuccess {Object} city City's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 City not found.
 */
router.get('/:id',
  show)

/**
 * @api {put} /cities/:id Update city
 * @apiName UpdateCity
 * @apiGroup City
 * @apiParam name City's name.
 * @apiSuccess {Object} city City's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 City not found.
 */
router.put('/:id',
  body({ name }),
  update)

/**
 * @api {delete} /cities/:id Delete city
 * @apiName DeleteCity
 * @apiGroup City
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 City not found.
 */
router.delete('/:id',
  destroy)

export default router
