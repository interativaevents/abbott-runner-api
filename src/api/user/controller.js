import _ from 'lodash'
import { success, notFound } from '../../services/response/'
import { User } from '.'
import mongoose from 'mongoose';

export const index = ({ querymen: { query, select, cursor } }, res, next) => {
  User.find(query, select, cursor)
    .sort('-score')
    .populate('team').exec()
    .then((users) => users.map((user) => {
      user.view()
      user.team.createdAt = undefined;
      user.team.updatedAt = undefined;
      user.team.questions = undefined;
      user.team.name = undefined;
      user.team._id = undefined;
      user.team.__v = undefined;
      user.team.city = undefined;

      user.__v = undefined;
      return user;
    }))
    .then(success(res))
    .catch(next)
}

export const groupByCity = ({ params }, res, next) => {

  User.aggregate([
    {
      $lookup:
      {
        from: "cities",
        localField: "city",
        foreignField: "_id",
        as: "city"
      }
    },

    {
      $match: {
        "city.name": { $regex: '^((?!TEST).)*$' }
      }
    },

    {
      $group: {
        _id: {
          $arrayElemAt: ["$city.name", 0]
        },
        avg:{ $avg: "$score" },
        score: { $sum: "$score" }
      }
    },

    {
      $sort: { score: -1 }
    }
  ])
    .then(result => res.json(result))
    .catch(next)

}



export const show = ({ params }, res, next) =>
  User.findById(params.id)
    .then(notFound(res))
    .then((user) => user ? user.view() : null)
    .then(success(res))
    .catch(next)

export const getUsersByCityId = ({ params }, res, next) =>
  User.find({ city: params.cityId })
    .populate('team')
    .exec()
    .then(notFound(res))
    .then((users) => users.map((user) => user.view()))
    .then(success(res))
    .catch(next)

export const showMe = ({ user }, res) => {
  res.json(user.view(true))
}

export const create = ({ bodymen: { body } }, res, next) => {

  User.create(body)
    .then((user) => user.view(true))
    .then(success(res, 201))
    .catch((err) => {
      /* istanbul ignore else */
      if (err.name === 'MongoError' && err.code === 11000) {
        res.status(409).json({
          valid: false,
          param: 'email',
          message: 'email already registered'
        })
      } else {
        next(err)
      }
    })
}


export const update = ({ bodymen: { body }, params, user }, res, next) =>
  User.findById(params.id === 'me' ? user.id : params.id)
    .then(notFound(res))
    .then((result) => {
      if (!result) return null
      const isAdmin = user.role === 'admin'
      const isSelfUpdate = user.id === result.id
      if (!isSelfUpdate && !isAdmin) {
        res.status(401).json({
          valid: false,
          message: 'You can\'t change other user\'s data'
        })
        return null
      }
      return result
    })
    .then((user) => user ? _.merge(user, body).save() : null)
    .then((user) => user ? user.view(true) : null)
    .then(success(res))
    .catch(next)

export const updatePassword = ({ bodymen: { body }, params, user }, res, next) =>
  User.findById(params.id === 'me' ? user.id : params.id)
    .then(notFound(res))
    .then((result) => {
      if (!result) return null
      const isSelfUpdate = user.id === result.id
      if (!isSelfUpdate) {
        res.status(401).json({
          valid: false,
          param: 'password',
          message: 'You can\'t change other user\'s password'
        })
        return null
      }
      return result
    })
    .then((user) => user ? user.set({ password: body.password }).save() : null)
    .then((user) => user ? user.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  User.findById(params.id)
    .then(notFound(res))
    .then((user) => user ? user.remove() : null)
    .then(success(res, 204))
    .catch(next)


export const createMany = ({ body }, res, next) => {

  User.create(body)
    .then((user) => user)
    .then(success(res, 201))
    .catch((err) => {
      /* istanbul ignore else */
      if (err.name === 'MongoError' && err.code === 11000) {
        res.status(409).json({
          valid: false,
          param: 'email',
          message: 'email already registered'
        })
      } else {
        next(err)
      }
    })
}
