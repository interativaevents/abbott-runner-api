import crypto from 'crypto'
import bcrypt from 'bcrypt-nodejs'
import bluebird from 'bluebird'
import mongoose, { Schema } from 'mongoose'
import mongooseKeywords from 'mongoose-keywords'
import { env } from '../../config'

const roles = ['user', 'admin']
const compare = require('bluebird').promisify(bcrypt.compare);

const userSchema = new Schema({
  email: {
    type: String,
    match: /^\S+@\S+\.\S+$/,
    required: false,
    unique: true,
    trim: true,
    lowercase: true,
    sparse: true
  },
  password: {
    type: String,
    required: false,
    minlength: 6
  },
  name: {
    type: String,
    index: true,
    //trim: true
  },
  role: {
    type: String,
    enum: roles,
    default: 'user'
  },
  team: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Team'
  },
  cities: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'City'
  }],
  score: { type: Number, default: 0 },
  itens: [Number],
  city: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'City'
  },
  time: {
    type: Number,
    default: 0
  },
  answer:{
    type:mongoose.Schema.Types.ObjectId,
    ref:"Answer"
  }
},
  {
    timestamps: true
  })


userSchema.pre('save', function (next) {
  if (!this.isModified('password')) return next()

  /* istanbul ignore next */
  const rounds = env === 'test' ? 1 : 9
  if (!!this.password) {
    bcrypt.hash(this.password, null, null, (err, hash) => {
      /* istanbul ignore next */
      if (err) return next(err)
      this.password = hash
      next()
    })
  } else {
    next()
  }
})

userSchema.methods = {
  view(full) {
    let view = {}
    let fields = ['id', 'name', 'score', 'team', 'city', 'itens', 'time','cities','answer']

    if (full) {
      fields = [...fields, 'email', 'createdAt']
    }

    fields.forEach((field) => { view[field] = this[field] })

    return view
  },

  authenticate(password) {
    return compare(password, this.password).then((valid) => valid ? this : false)
  }
}

userSchema.statics = {
  roles
}

userSchema.plugin(mongooseKeywords, { paths: ['name'] })

const model = mongoose.model('User', userSchema)

export const schema = model.schema
export default model
