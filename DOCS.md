# abbott-runner-api v0.0.0



- [Answer](#answer)
	- [Create answer](#create-answer)
	- [Delete answer](#delete-answer)
	- [Retrieve answer](#retrieve-answer)
	- [Retrieve answers](#retrieve-answers)
	- [Update answer](#update-answer)
	
- [Auth](#auth)
	- [Authenticate](#authenticate)
	
- [City](#city)
	- [Create many cities](#create-many-cities)
	- [Create city](#create-city)
	- [Delete city](#delete-city)
	- [Retrieve cities](#retrieve-cities)
	- [Retrieve city](#retrieve-city)
	- [Update city](#update-city)
	
- [Question](#question)
	- [Create question](#create-question)
	- [Delete question](#delete-question)
	- [Retrieve question](#retrieve-question)
	- [Retrieve questions](#retrieve-questions)
	- [Update question](#update-question)
	
- [Scores](#scores)
	- [Retrieve scores by city](#retrieve-scores-by-city)
	
- [Team](#team)
	- [Create team](#create-team)
	- [Delete team](#delete-team)
	- [verify if team have questions by Team id](#verify-if-team-have-questions-by-team-id)
	- [Retrieve team give a City ID](#retrieve-team-give-a-city-id)
	- [Retrieve teams](#retrieve-teams)
	- [Update team](#update-team)
	
- [User](#user)
	- [Create user](#create-user)
	- [Delete user](#delete-user)
	- [Retrieve current user](#retrieve-current-user)
	- [Retrieve user](#retrieve-user)
	- [Retrieve users](#retrieve-users)
	- [Update password](#update-password)
	- [Update user](#update-user)
	


# Answer

## Create answer



	POST /answers


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| userId			| 			|  <p>Answer's userId.</p>							|
| questionId			| 			|  <p>Answer's questionId.</p>							|
| alternativeId			| 			|  <p>Answer's alternativeId.</p>							|
| teamId			| 			|  <p>Answer's teamId</p>							|
| cityId			| 			|  <p>Answer's cityId</p>							|
| time			| 			|  <p>Answer's time</p>							|
| itens			| 			|  <p>Answer's itens</p>							|

## Delete answer



	DELETE /answers/:id


## Retrieve answer



	GET /answers/:id


## Retrieve answers



	GET /answers


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update answer



	PUT /answers/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| userId			| 			|  <p>Answer's userId.</p>							|
| questionId			| 			|  <p>Answer's questionId.</p>							|
| alternativeId			| 			|  <p>Answer's alternativeId.</p>							|
| teamId			| 			|  <p>Answer's teamId</p>							|
| cityId			| 			|  <p>Answer's cityId</p>							|
| time			| 			|  <p>Answer's time</p>							|
| itens			| 			|  <p>Answer's itens</p>							|

# Auth

## Authenticate



	POST /auth

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| Authorization			| String			|  <p>Basic authorization with email and password.</p>							|

### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Master access_token.</p>							|

# City

## Create many cities



	POST /cities/all


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| Names			| 			|  <p>City's names.</p>							|

## Create city



	POST /cities


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| name			| 			|  <p>City's name.</p>							|

## Delete city



	DELETE /cities/:id


## Retrieve cities



	GET /cities


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Retrieve city



	GET /cities/:id


## Update city



	PUT /cities/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| name			| 			|  <p>City's name.</p>							|

# Question

## Create question



	POST /questions


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin access token.</p>							|
| city			| 			|  <p>Question's city.</p>							|
| title			| 			|  <p>Question's title.</p>							|
| value			| 			|  <p>Question's value.</p>							|
| alternatives			| 			|  <p>Question's alternatives.</p>							|

## Delete question



	DELETE /questions/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin access token.</p>							|

## Retrieve question



	GET /questions/:id


## Retrieve questions



	GET /questions


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update question



	PUT /questions/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin access token.</p>							|
| city			| 			|  <p>Question's city.</p>							|
| title			| 			|  <p>Question's title.</p>							|
| value			| 			|  <p>Question's value.</p>							|
| alternatives			| 			|  <p>Question's alternatives.</p>							|

# Scores

## Retrieve scores by city



	GET /answers/score/:cityId


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

# Team

## Create team



	POST /teams


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| name			| 			|  <p>Team's name.</p>							|
| city			| 			|  <p>Team's city.</p>							|
| questions			| 			|  <p>Team's questions.</p>							|

## Delete team



	DELETE /teams/:id


## verify if team have questions by Team id



	GET /teams/isdone/:teamId


## Retrieve team give a City ID



	GET /teams/city/:cityId


## Retrieve teams



	GET /teams


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update team



	PUT /teams/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| name			| 			|  <p>Team's name.</p>							|
| city			| 			|  <p>Team's city.</p>							|
| questions			| 			|  <p>Team's questions.</p>							|

# User

## Create user



	POST /users


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Master access_token.</p>							|
| email			| String			|  <p>User's email.</p>							|
| password			| String			|  <p>User's password.</p>							|
| name			| String			| **optional** <p>User's name.</p>							|
| picture			| String			| **optional** <p>User's picture.</p>							|
| role			| String			| **optional** <p>User's picture.</p>							|

## Delete user



	DELETE /users/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|

## Retrieve current user



	GET /users/me


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|

## Retrieve user



	GET /users/:id


## Retrieve users



	GET /users


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update password



	PUT /users/:id/password

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| Authorization			| String			|  <p>Basic authorization with email and password.</p>							|

### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| password			| String			|  <p>User's new password.</p>							|

## Update user



	PUT /users/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|
| name			| String			| **optional** <p>User's name.</p>							|
| picture			| String			| **optional** <p>User's picture.</p>							|


