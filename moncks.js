//Criar cidaes no banco
{
    "cities":[
        { "name": "CHICAGO" },
        { "name": "TORONTO" },
        { "name": "CIDADE DO MEXICO" },
        { "name": "ARGENTINA" },
        { "name": "COLOMBIA" },
        { "name": "MIAMI" },
        { "name": "FLORIANOPOLIS" }
    ]
}
{
    "cities":[
        {
            "name": "FLORIANOPOLIS (TEST)"
        },
        {
            "name": "FLORIANOPOLIS (BONUS)"
        },


        {
            "name": "MIAMI (TEST)"
        },
        {
            "name": "MIAMI (BONUS)"
        },


        {
            "name": "COLOMBIA (TEST)"
        },
        {
            "name": "COLOMBIA (BONUS)"
        },


        {
            "name": "ARGENTINA (TEST)"
        },
        {
            "name": "ARGENTINA (BONUS)"
        },


        {
            "name": "CIDADE DO MEXICO (TEST)"
        },
        {
            "name": "CIDADE DO MEXICO (BONUS)"
        },


        {
            "name": "TORONTO (TEST)"
        },
        {
            "name": "TORONTO (BONUS)"
        },


        {
            "name": "CHICAGO (TEST)"
        },
        {
            "name": "CHICAGO (BONUS)"
        }
    ]
}

//cria usuarios no banco
[
    {
        "name": "USER CHICAGO",
        "email": "chicago@abbott.com",
        "password": "vvbb33",
        "cities": [
            "58a76ca4741fe847335bef68",
            "58b89f1df4df570668e10fdf",
            "58b89f1df4df570668e10fe0"],
        "role": "admin"
    },
    {
        "name": "USER TORONTO",
        "email": "toronto@abbott.com",
        "password": "pphh11",
        "cities": [
            "58a76ca4741fe847335bef69",
            "58b89f1df4df570668e10fdd",
            "58b89f1df4df570668e10fde"],
        "role": "admin"
    },
    {
        "name": "USER CIDADE DO MEXICO",
        "email": "cidadedomexico@abbott.com",
        "password": "kkdd55",
        "cities": [
            "58a76ca4741fe847335bef6a",
            "58b89f1df4df570668e10fdb",
            "58b89f1df4df570668e10fdc"],
        "role": "admin"
    },
    {
        "name": "USER ARGENTINA",
        "email": "argentina@abbott.com",
        "password": "ppdd44",
        "cities": [
            "58a76ca4741fe847335bef6b",
            "58b89f1df4df570668e10fd9",
            "58b89f1df4df570668e10fda"],
        "role": "admin"
    },
    {
        "name": "USER COLOMBIA",
        "email": "colombia@abbott.com",
        "password": "eezz55",
        "cities": [
            "58a76ca4741fe847335bef6c",
            "58b89f1df4df570668e10fd7",
            "58b89f1df4df570668e10fd8"],
        "role": "admin"
    },
    {
        "name": "USER MIAMI",
        "email": "miami@abbott.com",
        "password": "iivv99",
        "cities": [
            "58a76ca4741fe847335bef6d",
            "58b89f1df4df570668e10fd5",
            "58b89f1df4df570668e10fd6"],
        "role": "admin"
    },
    {
        "name": "USER FLORIANOPOLIS",
        "email": "florianopolis@abbott.com",
        "password": "qqgg66",
        "cities": [
            "58a76ca4741fe847335bef6e",
            "58b89f1df4df570668e10fd3",
            "58b89f1df4df570668e10fd4"],
        "role": "admin"
    },
    {
        "name": "USER ADMIN",
        "email": "admin@admin.com",
        "password": "berto2003",
        "cities": [
            "58a76ca4741fe847335bef6b",
            "58b89f1df4df570668e10fda",
            "58b89f1df4df570668e10fd9",
            "58a76ca4741fe847335bef68",
            "58b89f1df4df570668e10fe0",
            "58b89f1df4df570668e10fdf",
            "58a76ca4741fe847335bef6a",
            "58b89f1df4df570668e10fdc",
            "58b89f1df4df570668e10fdb",
            "58a76ca4741fe847335bef6c",
            "58b89f1df4df570668e10fd8",
            "58b89f1df4df570668e10fd7",
            "58a76ca4741fe847335bef6e",
            "58b89f1df4df570668e10fd4",
            "58b89f1df4df570668e10fd3",
            "58a76ca4741fe847335bef6d",
            "58b89f1df4df570668e10fd6",
            "58b89f1df4df570668e10fd5",
            "58a76ca4741fe847335bef69",
            "58b89f1df4df570668e10fde",
            "58b89f1df4df570668e10fdd"
        ],
        "role": "admin"
    }
]




{
    "cities":[

        {
            "_id": "58a76ca4741fe847335bef68",
            "name": "CHICAGO"
        },


        {
            "_id": "58a76ca4741fe847335bef69",
            "name": "TORONTO"
        },


        {
            "_id": "58a76ca4741fe847335bef6a",
            "name": "CIDADE DO MEXICO"
        },


        {
            "_id": "58a76ca4741fe847335bef6b",
            "name": "ARGENTINA"
        },


        {
            "_id": "58a76ca4741fe847335bef6c",
            "name": "COLOMBIA"
        },


        {
            "_id": "58a76ca4741fe847335bef6d",
            "name": "MIAMI"
        },

        {
            "_id": "58a76ca4741fe847335bef6e",
            "name": "FLORIANOPOLIS"
        }
    ]
}



db.users.aggregate([
    {
        $project: {
            name: 1
        }
    },
    {
        $lookup:
        {
            from: "cities",
            localField: "cities",
            foreignField: "_id",
            as: "result"
        }
    }
])

db.users.aggregate([
    {
        $lookup:
        {
            from: "cities",
            localField: "cities",
            foreignField: "_id",
            as: "result"
        }
    }, {
        $project: {
            name: 1,
            'result.name': 1,
            _id: 0
        }
    }
])



//reports
db.answers.aggregate([
    //--Users
    {
        $lookup:
        {
            from: "users",
            localField: "userId",
            foreignField: "_id",
            as: "user"
        }

    }
    ,//--Questions
    {
        $lookup:
        {
            from: "questions",
            localField: "questionId",
            foreignField: "_id",
            as: "question"
        }
    }
    ,//--Cities
    {
        $lookup:
        {
            from: "cities",
            localField: "cityId",
            foreignField: "_id",
            as: "city_name"
        }
    }
    ,//Teams
    {
        $lookup:
        {
            from: "teams",
            localField: "teamId",
            foreignField: "_id",
            as: "team_name"
        }
    }
    ,//----
    {
        $project:
        {
            user:
            {
                $arrayElemAt: ["$user.name", 0]
            },
            role:
            {
                $arrayElemAt: ["$user.role", 0]
            },
            score: 1,
            itens: 1,
            isRight: 1,
            alternativeId: 1,
            alternativeNumber: 1,
            city_name:
            {
                $arrayElemAt: ["$city_name.name", 0]
            }
            ,
            team_name:
            {
                $arrayElemAt: ["$team_name.name", 0]
            }
            ,
            question_title:
            {
                $arrayElemAt: ["$question.title", 0]
            },
            question_alternatives:
            {
                $arrayElemAt: ["$question.alternatives", 0]
            }


        }
    }
    ,//------
    {
        $out: "reports"
    }

])

mongodump--host 138.197.122.91 - d abbott- runner - api--port 27017  --out mongodump



[ObjectId("58a76ca4741fe847335bef6b"),
ObjectId("58b89f1df4df570668e10fda"),
ObjectId("58b89f1df4df570668e10fd9"),
ObjectId("58a76ca4741fe847335bef68"),
ObjectId("58b89f1df4df570668e10fe0"),
ObjectId("58b89f1df4df570668e10fdf"),
ObjectId("58a76ca4741fe847335bef6a"),
ObjectId("58b89f1df4df570668e10fdc"),
ObjectId("58b89f1df4df570668e10fdb"),
ObjectId("58a76ca4741fe847335bef6c"),
ObjectId("58b89f1df4df570668e10fd8"),
ObjectId("58b89f1df4df570668e10fd7"),
ObjectId("58a76ca4741fe847335bef6e"),
ObjectId("58b89f1df4df570668e10fd4"),
ObjectId("58b89f1df4df570668e10fd3"),
ObjectId("58a76ca4741fe847335bef6d"),
ObjectId("58b89f1df4df570668e10fd6"),
ObjectId("58b89f1df4df570668e10fd5"),
ObjectId("58a76ca4741fe847335bef69"),
ObjectId("58b89f1df4df570668e10fde"),
    ObjectId("58b89f1df4df570668e10fdd")]

["58a76ca4741fe847335bef6b",
"58b89f1df4df570668e10fda",
"58b89f1df4df570668e10fd9",
"58a76ca4741fe847335bef68",
"58b89f1df4df570668e10fe0",
"58b89f1df4df570668e10fdf",
"58a76ca4741fe847335bef6a",
"58b89f1df4df570668e10fdc",
"58b89f1df4df570668e10fdb",
"58a76ca4741fe847335bef6c",
"58b89f1df4df570668e10fd8",
"58b89f1df4df570668e10fd7",
"58a76ca4741fe847335bef6e",
"58b89f1df4df570668e10fd4",
"58b89f1df4df570668e10fd3",
"58a76ca4741fe847335bef6d",
"58b89f1df4df570668e10fd6",
"58b89f1df4df570668e10fd5",
"58a76ca4741fe847335bef69",
"58b89f1df4df570668e10fde",
    "58b89f1df4df570668e10fdd"]

db.getCollection('score').aggregate([{
    $lookup:
    {
        from: "user",
        localField: "user",
        foreignField: "_id",
        as: "user"
    }
},
{
    $match: { group: ObjectId("582c732d6790732c180d1beb") }
},
{
    $sort: { createdAt: -1 }
},
{
    $project: {
        name: {
            $arrayElemAt: ["$user.name", 0]
        },
        department: {
            $arrayElemAt: ["$user.department", 0]
        },
        upi: {
            $arrayElemAt: ["$user.upi", 0]
        },
        score: 1
    }

},
{
    $out: "reports"
}
])


//correção DX test para DX

db.users.updateMany(
    { team: ObjectId("58b89f1df4df570668e10fe5") },
    { $set: { team: ObjectId("58acb8433e289663dc93d70a"), city: ObjectId("58a76ca4741fe847335bef6d") } },
    { many: true }
)


db.answers.updateMany(
    { teamId: ObjectId("58b89f1df4df570668e10fe5") },
    { $set: { teamId: ObjectId("58acb8433e289663dc93d70a"), cityId: ObjectId("58a76ca4741fe847335bef6d") } },
    { many: true }
)


var questiosT1 = [
    ObjectId("58bda18535c92f1634d808ad"),
    ObjectId("58bda18535c92f1634d808ac"),
    ObjectId("58bda18535c92f1634d808ab"),
    ObjectId("58bda18535c92f1634d808aa"),
    ObjectId("58bda18535c92f1634d808a9"),
    ObjectId("58bda18535c92f1634d808a8"),
    ObjectId("58bda18535c92f1634d808a7"),
    ObjectId("58bda18535c92f1634d808a6"),
    ObjectId("58bda18535c92f1634d808a5"),
    ObjectId("58bda18535c92f1634d808a4"),
    ObjectId("58bda18535c92f1634d808a3"),
    ObjectId("58bda18535c92f1634d808a2"),
    ObjectId("58bda18535c92f1634d808a1"),
    ObjectId("58bda18535c92f1634d808a0"),
    ObjectId("58bda18535c92f1634d8089f")]



var answersT1 = [
    ObjectId("58c150b9b3e453349d7bc9ea"),
    ObjectId("58c15166b3e453349d7bc9ee"),
    ObjectId("58c151f1b3e453349d7bc9f2"),
    ObjectId("58c157aeb3e453349d7bc9fc"),
    ObjectId("58c15817b3e453349d7bca00"),
    ObjectId("58c1587db3e453349d7bca04"),
    ObjectId("58c158fcb3e453349d7bca08"),
    ObjectId("58c1595db3e453349d7bca0c"),
    ObjectId("58c159b7b3e453349d7bca10"),
    ObjectId("58c15a12b3e453349d7bca13"),
    ObjectId("58c15a64b3e453349d7bca17"),
    ObjectId("58c15adbb3e453349d7bca1c"),
    ObjectId("58c15b37b3e453349d7bca22"),
    ObjectId("58c15b9ab3e453349d7bca28"),
    ObjectId("58c15c04b3e453349d7bca30")]


["58a76ca4741fe847335bef6b",
"58b89f1df4df570668e10fda",
"58a76ca4741fe847335bef6e",
"58b89f1df4df570668e10fd4",
"58a76ca4741fe847335bef69",
"58b89f1df4df570668e10fde",
"58a76ca4741fe847335bef6c",
"58b89f1df4df570668e10fd8",
"58a76ca4741fe847335bef6d",
"58b89f1df4df570668e10fd6",
"58a76ca4741fe847335bef6a",
    "58b89f1df4df570668e10fdc"]

db.users.aggregate([
    {
        $lookup:
        {
            from: "user",
            localField: "user",
            foreignField: "_id",
            as: "user"
        }
    },
    {
        $match: { group: ObjectId("582c732d6790732c180d1beb") }
    },
    {
        $group: {
            _id: { city: "$city" },
            score: { $sum: "$score" }
        }
    }

])